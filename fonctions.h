#ifndef FONCTION_H
#define FONCTION_H

#include <iostream>
#include <array>
#include <algorithm>


using tableau = std::array<int, 6>;

namespace utils
{
	constexpr int sum(const tableau& t)
	{
		int val = 0;

		for (int i : t)
			val += i;

		return val;
	}
	
	constexpr int max(const tableau& t)
	{
		int val = 0;

		for (int i : t)
			if (i > val)
				val = i;

		return val;
	}

	constexpr tableau inverse(const tableau& t)
	{
		tableau t1(t);

		for (int i = 0; i < t.size(); i++)
			t1[t.size() - 1 - i] = t[i];

		return t1;
	}
	
	constexpr tableau multiply(const tableau& t, const int x)
	{
		tableau t1(t);

		for (int& i : t1)
			i *= x;

		return t1;
	}

	constexpr tableau unique(const tableau& t)
	{
		tableau t1(t);

		for (int i = 0; i < t1.size(); i++)
		{
			bool var = false;

			for (int j = i + 1; j < t1.size(); j++)
			{
				if (t1[j] == t1[i] && t1[j])
				{
					t1[j] = 0;
					var = true;
				}
			}

			if (var)
				t1[i] = 0;
		}

		return t1;
	}
}

#endif
