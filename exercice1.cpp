#include "fonctions.h"

int main() {
    constexpr tableau mon_tableau{ 1, 3, 2, 5, 3, 2 };

    // calcule la somme des éléments
    constexpr auto s{ utils::sum(mon_tableau) };

    // recherche la valeur maximale
    constexpr auto m{ utils::max(mon_tableau) };

    // inverse l'ordre des éléments du tableau (le premier devient le dernier, etc)
    constexpr auto t2{ utils::inverse(mon_tableau) };

    // multiplie chaque élément par une valeur constante
    constexpr tableau mon_tablau_multiplie = utils::multiply(mon_tableau, 2);

    // supprime les doublons
    constexpr auto  t3{ utils::unique(mon_tableau) };
}