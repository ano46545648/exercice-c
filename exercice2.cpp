#include <array>
#include <string>
#include <tuple>
#include <iostream>

using namespace std::string_literals;

void print()
{
    std::cout << "unknown type";
}

void print(bool argument)
{
    if (argument)
        std::cout << "bool : true" << std::endl;
    else
        std::cout << "bool : false" << std::endl;
}

template<typename T>
void print(const T& argument)
{
    std::cout << typeid(T).name() << " : " << argument << std::endl;
}

template<typename T, size_t size>
void print(const std::array<T, size>& tableau)
{
    std::cout << "std::array<" << typeid(T).name() << ", " << size << "> :";
    for (const T& i : tableau)
        std::cout << " " << i;
    std::cout << std::endl;
}

template<typename T, typename T2, typename T3>
void print(const std::tuple<T, T2, T3>& tuple)
{
    std::cout << "std::tuple<" << typeid(T).name() << ", " << typeid(T2).name() << ", " << typeid(T3).name() << "> : ";
    std::cout << std::get<0>(tuple) << " " << std::get<1>(tuple) << " " << std::get<2>(tuple) << std::endl;
}

int main() {
    print(true);
    print(123ul);
    print(123.456);
    print("hello!"s);

    print(std::make_tuple(123, 456, "hello"s));
    print(std::to_array({ 0, 2, 1, 3 }));

    print(); // affiche "unknown type"
}